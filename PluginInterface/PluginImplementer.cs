﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;
using System.Drawing;

namespace PluginInterface
{
    public interface PluginImplementer
    {
        List<System.Object> listGUIObjects();
        System.Drawing.Size RecomendedFrameSize();
        void SetupGUI_MainTab(TabPage tab);
        void SetupGUI_Objects();
        void InitializeComponentDLL();
    }
}
