﻿namespace DrawingListTool
{
    partial class UserControl1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Start = new System.Windows.Forms.Button();
            this.chk_Rename = new System.Windows.Forms.CheckBox();
            this.chk_ShowProfile = new System.Windows.Forms.CheckBox();
            this.chk_ShowScale = new System.Windows.Forms.CheckBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // btn_Start
            // 
            this.btn_Start.Location = new System.Drawing.Point(3, 108);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.Size = new System.Drawing.Size(160, 60);
            this.btn_Start.TabIndex = 0;
            this.btn_Start.Text = "Start";
            this.btn_Start.UseVisualStyleBackColor = true;
            // 
            // chk_Rename
            // 
            this.chk_Rename.AutoSize = true;
            this.chk_Rename.Location = new System.Drawing.Point(3, 3);
            this.chk_Rename.Name = "chk_Rename";
            this.chk_Rename.Size = new System.Drawing.Size(251, 29);
            this.chk_Rename.TabIndex = 1;
            this.chk_Rename.Text = "Rename by Main Part";
            this.chk_Rename.UseVisualStyleBackColor = true;
            // 
            // chk_ShowProfile
            // 
            this.chk_ShowProfile.AutoSize = true;
            this.chk_ShowProfile.Location = new System.Drawing.Point(3, 38);
            this.chk_ShowProfile.Name = "chk_ShowProfile";
            this.chk_ShowProfile.Size = new System.Drawing.Size(164, 29);
            this.chk_ShowProfile.TabIndex = 2;
            this.chk_ShowProfile.Text = "Show Profile";
            this.chk_ShowProfile.UseVisualStyleBackColor = true;
            // 
            // chk_ShowScale
            // 
            this.chk_ShowScale.AutoSize = true;
            this.chk_ShowScale.Location = new System.Drawing.Point(3, 73);
            this.chk_ShowScale.Name = "chk_ShowScale";
            this.chk_ShowScale.Size = new System.Drawing.Size(157, 29);
            this.chk_ShowScale.TabIndex = 3;
            this.chk_ShowScale.Text = "Show Scale";
            this.chk_ShowScale.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(3, 174);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(646, 61);
            this.progressBar1.TabIndex = 4;
            // 
            // UserControl1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.chk_ShowScale);
            this.Controls.Add(this.chk_ShowProfile);
            this.Controls.Add(this.chk_Rename);
            this.Controls.Add(this.btn_Start);
            this.Name = "UserControl1";
            this.Size = new System.Drawing.Size(650, 250);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Start;
        private System.Windows.Forms.CheckBox chk_Rename;
        private System.Windows.Forms.CheckBox chk_ShowProfile;
        private System.Windows.Forms.CheckBox chk_ShowScale;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}
