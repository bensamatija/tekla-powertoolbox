﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Tekla:
using T = Tekla.Structures;
using TSM = Tekla.Structures.Model;
using TSMUI = Tekla.Structures.Model.UI;
//using System.Collections;
using T3D = Tekla.Structures.Geometry3d;
//using Tekla.Technology.MacroSelector;
//using Tekla.Technology.Akit;
//using Tekla.Macros;
//using Tekla.Macros.Runtime;
//using Tekla.Technology.Scripting;

// Others:
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Configuration;
using System.Timers;
using System.Diagnostics;

using PluginInterface;

namespace DrawingListTool
{
    class DrawingListTool : PluginImplementer
    {
        public List<System.Object> listGUIObjects = new List<object>();

        List<object> PluginImplementer.listGUIObjects() { return listGUIObjects; }

        public System.Drawing.Size RecomendedFrameSize() { return new System.Drawing.Size(700, 500); }

        public void SetupGUI_MainTab(TabPage tab)
        {
            tab.Size = new System.Drawing.Size(40, 40);
            tab.Text = "Drawing List Tool";
            //tab.Name = "plugin name";
            tab.BackColor = Color.White;

        }

        public void SetupGUI_Objects()
        {
            this.btn_Start = new System.Windows.Forms.Button();
            this.chk_Rename = new System.Windows.Forms.CheckBox();
            this.chk_ShowProfile = new System.Windows.Forms.CheckBox();
            this.chk_ShowScale = new System.Windows.Forms.CheckBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            //this.SuspendLayout();
            // 
            // btn_Start
            // 
            this.btn_Start.Location = new System.Drawing.Point(2, 108);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.Size = new System.Drawing.Size(160, 60);
            this.btn_Start.TabIndex = 0;
            this.btn_Start.Text = "Start";
            this.btn_Start.UseVisualStyleBackColor = true;
            // 
            // chk_Rename
            // 
            this.chk_Rename.AutoSize = true;
            this.chk_Rename.Location = new System.Drawing.Point(3, 3);
            this.chk_Rename.Name = "chk_Rename";
            this.chk_Rename.Size = new System.Drawing.Size(251, 29);
            this.chk_Rename.TabIndex = 1;
            this.chk_Rename.Text = "Rename by Main Part";
            this.chk_Rename.UseVisualStyleBackColor = true;
            // 
            // chk_ShowProfile
            // 
            this.chk_ShowProfile.AutoSize = true;
            this.chk_ShowProfile.Location = new System.Drawing.Point(3, 38);
            this.chk_ShowProfile.Name = "chk_ShowProfile";
            this.chk_ShowProfile.Size = new System.Drawing.Size(164, 29);
            this.chk_ShowProfile.TabIndex = 2;
            this.chk_ShowProfile.Text = "Show Profile";
            this.chk_ShowProfile.UseVisualStyleBackColor = true;
            // 
            // chk_ShowScale
            // 
            this.chk_ShowScale.AutoSize = true;
            this.chk_ShowScale.Location = new System.Drawing.Point(3, 73);
            this.chk_ShowScale.Name = "chk_ShowScale";
            this.chk_ShowScale.Size = new System.Drawing.Size(157, 29);
            this.chk_ShowScale.TabIndex = 3;
            this.chk_ShowScale.Text = "Show Scale";
            this.chk_ShowScale.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(3, 174);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(646, 61);
            this.progressBar1.TabIndex = 4;


            // Add all the GUI Objects to the list:
            listGUIObjects.Add(btn_Start);
            listGUIObjects.Add(progressBar1);
            listGUIObjects.Add(chk_Rename);
            listGUIObjects.Add(chk_ShowProfile);
            listGUIObjects.Add(chk_ShowScale);
        }

        private System.Windows.Forms.Button btn_Start;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.CheckBox chk_Rename;
        private System.Windows.Forms.CheckBox chk_ShowProfile;
        private System.Windows.Forms.CheckBox chk_ShowScale;

        public void InitializeComponentDLL()
        {
            //ConnectToModel();

        }
    }
}
