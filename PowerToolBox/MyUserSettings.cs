﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Reflection;

/// <summary>
/// Settings for the main form
/// </summary>
public class MyUserSettings : ApplicationSettingsBase
{
    [UserScopedSetting]
    public Color BackgroundColor
    {
        get { return ((Color)this["BackgroundColor"]); }
        set { this["BackgroundColor"] = (Color)value; }
    }

    /// <summary>
    /// Save and Restore the main form position
    /// </summary>
    [UserScopedSetting]
    public Point WindowPosition
    {
        get { return ((Point)this["WindowPosition"]); }
        set { this["WindowPosition"] = (Point)value; }
    }
}
