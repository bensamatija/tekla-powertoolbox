﻿namespace Tekla___PowerToolBox
{
    partial class ToolBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToolBox));
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btn_RunMacro = new System.Windows.Forms.Button();
            this.chk_onTop = new System.Windows.Forms.CheckBox();
            this.Info = new System.Windows.Forms.Label();
            this.Tab_ControlBox = new System.Windows.Forms.TabControl();
            this.Tab_Modeling = new System.Windows.Forms.TabPage();
            this.Tab_Drawings = new System.Windows.Forms.TabPage();
            this.Tab_Plugins = new System.Windows.Forms.TabPage();
            this.Plugins_ControlBox = new System.Windows.Forms.TabControl();
            this.button1 = new System.Windows.Forms.Button();
            this.TextBox_StatusAndSearch = new System.Windows.Forms.TextBox();
            this.Tab_ControlBox.SuspendLayout();
            this.Tab_Plugins.SuspendLayout();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(350, 313);
            this.button2.Margin = new System.Windows.Forms.Padding(6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(150, 44);
            this.button2.TabIndex = 1;
            this.button2.Text = "Save XML";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(350, 369);
            this.button3.Margin = new System.Windows.Forms.Padding(6);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(150, 44);
            this.button3.TabIndex = 2;
            this.button3.Text = "Load XML";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn_RunMacro
            // 
            this.btn_RunMacro.Location = new System.Drawing.Point(188, 369);
            this.btn_RunMacro.Margin = new System.Windows.Forms.Padding(6);
            this.btn_RunMacro.Name = "btn_RunMacro";
            this.btn_RunMacro.Size = new System.Drawing.Size(150, 44);
            this.btn_RunMacro.TabIndex = 3;
            this.btn_RunMacro.Text = "Run Macro";
            this.btn_RunMacro.UseVisualStyleBackColor = true;
            this.btn_RunMacro.Click += new System.EventHandler(this.btn_RunMacro_Click);
            // 
            // chk_onTop
            // 
            this.chk_onTop.Checked = true;
            this.chk_onTop.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_onTop.Font = new System.Drawing.Font("Consolas", 7.875F);
            this.chk_onTop.Location = new System.Drawing.Point(350, 460);
            this.chk_onTop.Margin = new System.Windows.Forms.Padding(6);
            this.chk_onTop.Name = "chk_onTop";
            this.chk_onTop.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chk_onTop.Size = new System.Drawing.Size(160, 33);
            this.chk_onTop.TabIndex = 0;
            this.chk_onTop.Text = "Top";
            this.chk_onTop.UseVisualStyleBackColor = true;
            this.chk_onTop.CheckedChanged += new System.EventHandler(this.chk_onTop_CheckedChanged);
            // 
            // Info
            // 
            this.Info.AutoSize = true;
            this.Info.Font = new System.Drawing.Font("Consolas", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Info.Location = new System.Drawing.Point(402, 462);
            this.Info.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.Info.Name = "Info";
            this.Info.Size = new System.Drawing.Size(24, 26);
            this.Info.TabIndex = 5;
            this.Info.Text = "?";
            this.Info.Click += new System.EventHandler(this.Info_Click);
            // 
            // Tab_ControlBox
            // 
            this.Tab_ControlBox.Controls.Add(this.Tab_Modeling);
            this.Tab_ControlBox.Controls.Add(this.Tab_Drawings);
            this.Tab_ControlBox.Controls.Add(this.Tab_Plugins);
            this.Tab_ControlBox.Location = new System.Drawing.Point(0, 0);
            this.Tab_ControlBox.Name = "Tab_ControlBox";
            this.Tab_ControlBox.SelectedIndex = 0;
            this.Tab_ControlBox.Size = new System.Drawing.Size(570, 278);
            this.Tab_ControlBox.TabIndex = 6;
            // 
            // Tab_Modeling
            // 
            this.Tab_Modeling.Location = new System.Drawing.Point(8, 39);
            this.Tab_Modeling.Name = "Tab_Modeling";
            this.Tab_Modeling.Padding = new System.Windows.Forms.Padding(3);
            this.Tab_Modeling.Size = new System.Drawing.Size(554, 231);
            this.Tab_Modeling.TabIndex = 1;
            this.Tab_Modeling.Text = "Modeling";
            this.Tab_Modeling.UseVisualStyleBackColor = true;
            // 
            // Tab_Drawings
            // 
            this.Tab_Drawings.Location = new System.Drawing.Point(8, 39);
            this.Tab_Drawings.Name = "Tab_Drawings";
            this.Tab_Drawings.Padding = new System.Windows.Forms.Padding(3);
            this.Tab_Drawings.Size = new System.Drawing.Size(554, 231);
            this.Tab_Drawings.TabIndex = 2;
            this.Tab_Drawings.Text = "Drawings";
            this.Tab_Drawings.UseVisualStyleBackColor = true;
            // 
            // Tab_Plugins
            // 
            this.Tab_Plugins.Controls.Add(this.Plugins_ControlBox);
            this.Tab_Plugins.Location = new System.Drawing.Point(8, 39);
            this.Tab_Plugins.Name = "Tab_Plugins";
            this.Tab_Plugins.Padding = new System.Windows.Forms.Padding(3);
            this.Tab_Plugins.Size = new System.Drawing.Size(554, 231);
            this.Tab_Plugins.TabIndex = 0;
            this.Tab_Plugins.Text = "Plugins";
            this.Tab_Plugins.UseVisualStyleBackColor = true;
            // 
            // Plugins_ControlBox
            // 
            this.Plugins_ControlBox.ItemSize = new System.Drawing.Size(40, 40);
            this.Plugins_ControlBox.Location = new System.Drawing.Point(0, 0);
            this.Plugins_ControlBox.Name = "Plugins_ControlBox";
            this.Plugins_ControlBox.SelectedIndex = 0;
            this.Plugins_ControlBox.Size = new System.Drawing.Size(310, 231);
            this.Plugins_ControlBox.TabIndex = 2;
            this.Plugins_ControlBox.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(188, 313);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 44);
            this.button1.TabIndex = 0;
            this.button1.Text = "Load DLL";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // TextBox_StatusAndSearch
            // 
            this.TextBox_StatusAndSearch.Font = new System.Drawing.Font("Consolas", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_StatusAndSearch.Location = new System.Drawing.Point(8, 456);
            this.TextBox_StatusAndSearch.Name = "TextBox_StatusAndSearch";
            this.TextBox_StatusAndSearch.Size = new System.Drawing.Size(393, 32);
            this.TextBox_StatusAndSearch.TabIndex = 7;
            this.TextBox_StatusAndSearch.Click += new System.EventHandler(this.TextBox_StatusAndSearch_Click);
            // 
            // ToolBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(568, 502);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TextBox_StatusAndSearch);
            this.Controls.Add(this.Tab_ControlBox);
            this.Controls.Add(this.Info);
            this.Controls.Add(this.chk_onTop);
            this.Controls.Add(this.btn_RunMacro);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "ToolBox";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Form1";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFormClosing);
            this.Load += new System.EventHandler(this.MainFormLoad);
            this.Tab_ControlBox.ResumeLayout(false);
            this.Tab_Plugins.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btn_RunMacro;
        private System.Windows.Forms.CheckBox chk_onTop;
        private System.Windows.Forms.Label Info;
        private System.Windows.Forms.TabControl Tab_ControlBox;
        private System.Windows.Forms.TabPage Tab_Plugins;
        private System.Windows.Forms.TabPage Tab_Modeling;
        private System.Windows.Forms.TabPage Tab_Drawings;
        private System.Windows.Forms.TextBox TextBox_StatusAndSearch;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabControl Plugins_ControlBox;
    }
}

