﻿namespace Tekla___PowerToolBox
{
    partial class DeveloperInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeveloperInfoForm));
            this.devLabel1 = new System.Windows.Forms.Label();
            this.btn_devInfoOk = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // devLabel1
            // 
            this.devLabel1.AutoSize = true;
            this.devLabel1.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.devLabel1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.devLabel1.Location = new System.Drawing.Point(24, 17);
            this.devLabel1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.devLabel1.Name = "devLabel1";
            this.devLabel1.Size = new System.Drawing.Size(120, 26);
            this.devLabel1.TabIndex = 0;
            this.devLabel1.Text = "devLabel1";
            // 
            // btn_devInfoOk
            // 
            this.btn_devInfoOk.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btn_devInfoOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_devInfoOk.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_devInfoOk.Location = new System.Drawing.Point(116, 328);
            this.btn_devInfoOk.Margin = new System.Windows.Forms.Padding(6);
            this.btn_devInfoOk.Name = "btn_devInfoOk";
            this.btn_devInfoOk.Size = new System.Drawing.Size(120, 44);
            this.btn_devInfoOk.TabIndex = 1;
            this.btn_devInfoOk.Text = "OK";
            this.btn_devInfoOk.UseVisualStyleBackColor = false;
            this.btn_devInfoOk.Click += new System.EventHandler(this.btn_devInfoOk_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(100, 155);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(164, 164);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // DeveloperInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlText;
            this.ClientSize = new System.Drawing.Size(540, 484);
            this.ControlBox = false;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btn_devInfoOk);
            this.Controls.Add(this.devLabel1);
            this.Enabled = false;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "DeveloperInfoForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Developer Info";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.DeveloperInfoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label devLabel1;
        private System.Windows.Forms.Button btn_devInfoOk;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}