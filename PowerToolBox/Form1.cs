﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Tekla:
using T = Tekla.Structures;
using TSM = Tekla.Structures.Model;
using TSMUI = Tekla.Structures.Model.UI;
//using System.Collections;
using T3D = Tekla.Structures.Geometry3d;
//using Tekla.Technology.MacroSelector;
//using Tekla.Technology.Akit;
//using Tekla.Macros;
//using Tekla.Macros.Runtime;
//using Tekla.Technology.Scripting;

// Others:
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Configuration;
using System.Timers;
using System.Diagnostics;

using PluginInterface;

namespace Tekla___PowerToolBox
{
    public partial class ToolBox : Form
    {
        public ToolBox()
        {
            //ShowStartupWelcomeScreen("Show");
            InitializeComponent();

            // Auto Load Settings:
            SetStartupFormSize();           
            GetSetMacroDirectory();
            LoadFromXML(settingsFolderFullPath + "\\" + buttonPropertiesXML);
            this.Text = appName;
            StopDebugButtons();
            //LoadPluginsFromPluginsFolder();
            LoadPluginImplementer();
            //this.AutoScaleMode = AutoScaleMode.Inherit;
            //SetKeyboardHook();    // Used for keyboard shortcuts
            SetupOtherStartupThings();
            //ShowStartupWelcomeScreen("Close");
        }

        string appName = "";//"PowerToolBox";        
        public string _XS_MACRO_DIRECTORY = "";
        string settingsFolderFullPath = "";
        public string macrosmodeling = "modeling";
        //string macrodrawings = "drawings";
        //static string settingsFolder = "modeling\\MacroCaller_Settings";
        //static string settingsFolder = "modeling\\MacroCaller\\MacroCaller_Settings";
        static string settingsFolder = "modeling\\PowerToolBox\\PowerToolBox_Settings\\";
        string buttonPropertiesXML = "PowerToolBox_Buttons.xml";

        MyUserSettings myUserSettings;
        KeyboardHook kbh = new KeyboardHook();
        public int numOfXmlButtons = 0;
        public System.Drawing.Size Tab_Modeling_Saved_MinFormSize;
        public System.Drawing.Size Tab_Drawings_Saved_MinFormSize;
        public System.Drawing.Size Tab_Plugins_Saved_MinFormSize;

        private void SetupOtherStartupThings()
        {
            Tab_Plugins.Paint += Tab_Plugins_Invalidated;
            Tab_Modeling.Paint += Tab_Modeling_Paint;
            Tab_Drawings.Paint += Tab_Drawings_Paint;
        }

        private void Tab_Drawings_Paint(object sender, PaintEventArgs e)
        {
            this.ClientSize = Tab_Plugins_Saved_MinFormSize + new System.Drawing.Size(3, 0);
            RepositionOtherGUIElements_BasedOnClientFormSize();
        }

        private void Tab_Modeling_Paint(object sender, PaintEventArgs e)
        {
            this.ClientSize = Tab_Plugins_Saved_MinFormSize + new System.Drawing.Size(3, 0);
            RepositionOtherGUIElements_BasedOnClientFormSize();
        }

        private void Tab_Plugins_Invalidated(object sender, EventArgs e)
        {
            if (buttonsList.Count != 0)
            {
                //LoadFromXML(settingsFolderFullPath + "\\" + buttonPropertiesXML);
                this.ClientSize = Tab_Plugins_Saved_MinFormSize + new System.Drawing.Size(3, 0);
                RepositionOtherGUIElements_BasedOnClientFormSize();
            }
        }

        /// <summary>
        /// Gets the directory our Tekla is looking for macros
        /// Advanced options
        /// </summary>
        public void GetSetMacroDirectory()
        {
            try
            {
                T.TeklaStructuresSettings.GetAdvancedOption("XS_MACRO_DIRECTORY", ref _XS_MACRO_DIRECTORY);
                settingsFolderFullPath = Path.GetFullPath(Path.Combine(_XS_MACRO_DIRECTORY, settingsFolder));
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message + "\n\nXS_MACRO_DIRECTORY not set properly,\nor Tekla Structures not running");

                // Set the blind file path so we can run the app without running Tekla:
                _XS_MACRO_DIRECTORY = "C:\\TeklaStructures\\21.0\\Environments\\Common\\macros";
                settingsFolderFullPath = "C:\\TeklaStructures\\21.0\\Environments\\Common\\macros\\modeling\\PowerToolBox\\PowerToolBox_Settings";
                //ExitApplication();
            }
        }

        private string GetApplicationRootFolder()
        {
            try
            {
                T.TeklaStructuresSettings.GetAdvancedOption("XS_MACRO_DIRECTORY", ref _XS_MACRO_DIRECTORY);
                string appRootFolder = Path.GetFullPath(Path.Combine(_XS_MACRO_DIRECTORY, "modeling\\PowerToolBox\\"));
                return appRootFolder;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message + "\n\nXS_MACRO_DIRECTORY not set properly,\nor Tekla Structures not running");
                _XS_MACRO_DIRECTORY = "C:\\TeklaStructures\\21.0\\Environments\\Common\\macros";
                string appRootFolder = Path.GetFullPath(Path.Combine(_XS_MACRO_DIRECTORY, "modeling\\PowerToolBox\\"));
                return appRootFolder;
            }

        }

        public static List<string> PropertyFileDirectories { get; set; }

        public int winW = 255, winH = 20;
        public int winHmin = 80; //100;
        public int maxRows = 1, maxCols = 4;
        ToolTip myToolTip = new ToolTip();
        ToolTip ToolTip1 = new ToolTip();

        public ButtonProperties b = new ButtonProperties();
        [XmlArray("ButtonsList")]
        List<ButtonProperties> buttonsList = new List<ButtonProperties>();
        [XmlArray("ButtonsListLoaded")]
        List<ButtonProperties> loadedList = new List<ButtonProperties>();

        private void SetStartupFormSize()
        {
            appName = "PowerToolBox " + GetAppVersion();
            this.ClientSize = new System.Drawing.Size(winW, winH + 60);
        }

        private void StopDebugButtons()
        {
            button2.Hide();
            button3.Hide();
            btn_RunMacro.Hide();
            button1.Hide();     // Load DLL
        }

        /// <summary>
        /// Button Run Macro
        /// This can be removed later
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_RunMacro_Click(object sender, EventArgs e)
        {
            string macroName = "Representation_standard.cs";
            RunThisMacro(macroName);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CreateXMLfile();
        }

        /// <summary>
        /// Creates .xml file to get the idea how it should look like:
        /// </summary>
        private void CreateXMLfile()
        {
            // Create List of Button properties:
            //buttonsList = new List<ButtonProperties>();
            buttonsList.Clear();

            // Create Buttons and Save them:

            b = new ButtonProperties();
            b.buttonName = "Button_1";
            b.macro = "DirectoryBrowser.cs";
            b.image = "default.jpg";
            b.row = 1;
            b.col = 1;
            buttonsList.Add(b);

            b = new ButtonProperties();
            b.buttonName = "Button_2";
            b.macro = "SKA_CreateSurfaceView.cs";
            b.image = "default.jpg";
            //string a = GetNameForImage(b.macro);         
            b.row = 1;
            b.col = 2;
            buttonsList.Add(b);

            b = new ButtonProperties();
            b.buttonName = "Button_3";
            b.macro = "SKA_InquireWeldMainPart.cs";
            b.image = "Capture.jpg";
            b.row = 2;
            b.col = 1;
            buttonsList.Add(b);

            b = new ButtonProperties();
            b.buttonName = "Button_4";
            b.macro = "SKA_RotateView_LeftSide.cs";
            b.image = "default.jpg";
            b.row = 2;
            b.col = 2;
            buttonsList.Add(b);

            // Check if the file exists:
            if (!Directory.Exists(settingsFolderFullPath))
            {
                System.IO.Directory.CreateDirectory(settingsFolderFullPath);
                SaveToXML(settingsFolderFullPath + "\\" + buttonPropertiesXML, buttonsList);
            }

            // Save New Button Properties to XML file:
            SaveToXML(settingsFolderFullPath + "\\" + buttonPropertiesXML, buttonsList);
        }

        /// <summary>
        /// Set up each button
        /// </summary>
        /// <param name="btn"></param>
        private void CreateDynamicButton(ButtonProperties btn, int numOfXmlButtons)
        {
            Button dynamicButton = new Button();

            //dynamicButton.Text = btn.macro.ToString().Remove(btn.macro.Length - 3);     // Predict that we have *.cs files only !
            dynamicButton.Name = btn.macro;
            int btnOffsetX = 0;
            int btnOffsetY = 0;
            dynamicButton.Height = 40;
            dynamicButton.Width = 40;
            string settingsFolderFullPath = Path.GetFullPath(Path.Combine(_XS_MACRO_DIRECTORY, settingsFolder));
            dynamicButton.Image = Image.FromFile(settingsFolderFullPath + "\\" + btn.image);
            dynamicButton.Image = ResizeImage(dynamicButton.Image, new Size(dynamicButton.Width - 0, dynamicButton.Height - 0));
            btn.pX = btn.col * dynamicButton.Width - dynamicButton.Width + btnOffsetX;
            btn.pY = btn.row * dynamicButton.Height - dynamicButton.Height + btnOffsetY;

            dynamicButton.Location = new Point(btn.pX, btn.pY);
            //dynamicButton.Font = new Font("Consolas", 8);

            dynamicButton.MouseEnter += new System.EventHandler(HoverMouseOverButton);
            dynamicButton.Click += new EventHandler(DynamicButton_Click);

            //Controls.Add(dynamicButton);              // Add to Normal Form window    
            //Tab_Custom.Controls.Add(dynamicButton);      // Add to Tab
            if (dynamicButton.Name.Contains("\\drawings\\"))
            {
                Tab_Drawings.Controls.Add(dynamicButton);      // Add to Tab
            }
            else
            {
                Tab_Modeling.Controls.Add(dynamicButton);      // Add to Tab
            }

            // Form Window Size:
            FormResizer(btn, dynamicButton, numOfXmlButtons);

            // Tab Resize:
            //TabResizer();
        }

        /// <summary>
        /// Resize the form based on the dynamic button's positions
        /// </summary>
        /// <param name="btn"></param>
        /// <param name="dynamicButton"></param>
        private void FormResizer(ButtonProperties btn, Button dynamicButton, int numOfXmlButtonsFull)
        {
            int btnOffsetX = 5;     // 15 => space for statusText
            int btnOffsetY = 40;    // 15 => space for statusText

            if (btn.row > maxRows)
            {
                maxRows = btn.row;
                winH = btn.row * dynamicButton.Height;
                //this.ClientSize = new System.Drawing.Size(winW, winH + btnOffsetY);
                //Tab_ControlBox.Height = this.ClientSize.Height - 15;
            }
            if (btn.col > maxCols)
            {
                maxCols = btn.col;
                winW = btn.col * dynamicButton.Width + btnOffsetX;
                //this.ClientSize = new System.Drawing.Size(winW, winH + btnOffsetY);
                //Tab_ControlBox.Width = this.ClientSize.Width + btnOffsetX - 2;
            }

            numOfXmlButtons++;
            if (numOfXmlButtons >= numOfXmlButtonsFull)
            {
                if (winH >= winHmin)        // If we don't have many buttons ignore the scaling and use preset size:
                {
                    numOfXmlButtons = 0;
                    print(Tab_ControlBox.Width + " " + Tab_ControlBox.Height);

                    this.ClientSize = new System.Drawing.Size(winW, winH + btnOffsetY);
                    Tab_ControlBox.Height = this.ClientSize.Height - 15;
                    this.ClientSize = new System.Drawing.Size(winW, winH + btnOffsetY);
                    Tab_ControlBox.Width = this.ClientSize.Width + btnOffsetX - 2;
                    TextBox_StatusAndSearch.Width = this.ClientSize.Width - 60;

                    //Tab_Plugins_Saved_MinFormSize = this.ClientSize;     // Save required form size, so we don't have to load xml file again
                    //PositionOtherGUI();
                }

                else
                {
                    RepositionOtherGUIElements_BasedOnClientFormSize();
                }
                //RepositionOtherGUIElements_BasedOnClientFormSize();
                Tab_Plugins_Saved_MinFormSize = this.ClientSize;     // Save required form size, so we don't have to load xml file again
                PositionOtherGUI();
            }
        }

        private void PositionOtherGUI()
        {
            chk_onTop.Location = new Point(this.ClientSize.Width - 80, this.ClientSize.Height - 15);
            Info.Location = new Point(this.ClientSize.Width - 55, this.ClientSize.Height - 15);
            TextBox_StatusAndSearch.Location = new Point(0, this.ClientSize.Height - 17);
            //Modeling_ControlBox.Size = this.ClientSize - new Size(5, 40);
        }

        private void FormSize_basedOn_TabControlBoxSubtab(System.Drawing.Size frameSize)
        {
            this.ClientSize = new System.Drawing.Size(frameSize.Width + 7, frameSize.Height + 40);
            RepositionOtherGUIElements_BasedOnClientFormSize();
        }

        private void RepositionOtherGUIElements_BasedOnClientFormSize()
        {
            chk_onTop.Location = new Point(this.ClientSize.Width - 80, this.ClientSize.Height - 15);
            Info.Location = new Point(this.ClientSize.Width - 55, this.ClientSize.Height - 15);
            TextBox_StatusAndSearch.Location = new Point(0, this.ClientSize.Height - 17);
            TextBox_StatusAndSearch.Width = this.ClientSize.Width - 60;

            RescaleOtherGUILemenets_BasedOnClientFormSize();
        }

        private void RescaleOtherGUILemenets_BasedOnClientFormSize()
        {
            Tab_ControlBox.Size = new System.Drawing.Size(this.ClientSize.Width - 0, this.ClientSize.Height - 15);

        }

        /// <summary>
        /// Resize the button's image
        /// </summary>
        /// <param name="imgToResize"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        private static Image ResizeImage(Image imgToResize, Size size)
        {
            //Image img = new Bitmap(imgToResize, size);
            //Bitmap bitmap = new Bitmap(imgToResize);
            //bitmap.SetResolution(imgToResize.HorizontalResolution, imgToResize.VerticalResolution);
            //using (var graphics = Graphics.FromImage(imgToResize))
            //{
            //    graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            //}
            return (Image)(new Bitmap(imgToResize, size));
        }

        /// <summary>
        /// This is the Load Button
        /// We can remove this later
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            // This is for reseting the max values for every load:
            maxCols = 4;
            maxRows = 0;
            //// Load the properties from XML to the Object:
            string _XS_MACRO_DIRECTORY = "";
            T.TeklaStructuresSettings.GetAdvancedOption("XS_MACRO_DIRECTORY", ref _XS_MACRO_DIRECTORY);
            string settingsXMLFullPath = Path.GetFullPath(Path.Combine(_XS_MACRO_DIRECTORY, settingsFolder, buttonPropertiesXML));
            LoadFromXML(settingsXMLFullPath);
        }

        /// <summary>
        /// This is the event that happens after we click on dynamicly created button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DynamicButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Identify which button was clicked:
                Button button = sender as Button;
                this.Enabled = false;

                RunThisMacro(button.Name);

                // Dispose all click events that happened during that time:
                Application.DoEvents();

                this.Enabled = true;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        /// <summary>
        /// Serializing buttons properties
        /// </summary>
        /// <param name="buttonPropertiesXML"></param>
        /// <param name="buttonsList"></param>
        private void SaveToXML(string buttonPropertiesXML, List<ButtonProperties> buttonsList)
        {
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<ButtonProperties>));
                StreamWriter sw = new StreamWriter(buttonPropertiesXML);
                xmlSerializer.Serialize(sw, buttonsList);
                sw.Close();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        /// <summary>
        /// DeSerializing buttons properties
        /// </summary>
        /// <param name="buttonPropertiesXML"></param>
        private void LoadFromXML(string buttonPropertiesXML)
        {
            string debugTrace_line = null;
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<ButtonProperties>));
                StreamReader sr = new StreamReader(buttonPropertiesXML);
                loadedList = (List<ButtonProperties>)xmlSerializer.Deserialize(sr);
                if (loadedList.Count == 0)
                {
                    FormSize_basedOn_TabControlBoxSubtab(new System.Drawing.Size(250, winHmin));
                    return;
                }
                foreach (ButtonProperties buttonProperties in loadedList)
                {
                    debugTrace_line = buttonProperties.macro;       // for debugging purposes only
                    CreateDynamicButton(buttonProperties, loadedList.Count);
                }
                sr.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("A problem occurred while loading " + "PowerToolBox_Buttons.xml" + ".\n\n" + ex.Message + "\n\n" + debugTrace_line + "\n\n Application will exit.\n");
                //MessageBox.Show("A problem occurred while loading " + "PowerToolBox_Buttons.xml" + ".\n\n" + ex.Message + "\n\n" + debugTrace_line + "\n\n We will create new one for you.\n");
                //CreateXMLfile();
                ExitApplication();
            }
        }

        /// <summary>
        /// Run selected macro
        /// </summary>
        /// <param name="macroName"></param>
        private void RunThisMacro(string macroName)
        {
            try
            {
                //string selectedMacro = Path.GetFullPath(Path.Combine(_XS_MACRO_DIRECTORY, macrosmodeling, macroName));
                string selectedMacro = Path.GetFullPath(Path.Combine(_XS_MACRO_DIRECTORY, macroName));
                //string selectedMacro = Path.GetFullPath(_XS_MACRO_DIRECTORY + "\\" + macroName);
                selectedMacro = Path.Combine(selectedMacro);

                //if (System.IO.File.Exists(selectedMacro))
                if (1 == 1)          // We skip the file checking
                {
                    //string state = (MacroState.Active.Equals(false).ToString());
                    //if (state == "False")
                    //{
                    if (selectedMacro.Contains("\\drawings\\"))
                    {
                        Tekla.Structures.Model.Operations.Operation.RunMacro(@"../" + macroName);
                    }
                    else
                    {
                        Tekla.Structures.Model.Operations.Operation.RunMacro(@"/" + macroName);    // It automaticly goes to modeling directory. For Drawing macro do @"../drawings/"
                    }
                    //}
                    //else
                    //{
                    //    System.Windows.Forms.MessageBox.Show(macroName + "Macro " + selectedMacro, "is allready running", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    //}
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show(macroName + " not found, application stopped!\n\nCheck the files in " + selectedMacro, "Tekla Structures", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        /// <summary>
        /// Windows always on top checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chk_onTop_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_onTop.Checked.Equals(true))
            {
                this.TopMost = true;
            }
            else
            {
                this.TopMost = false;
            }
        }

        /// <summary>
        /// Text that appears after we hover the dynamic button with mouse
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HoverMouseOverButton(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            //print(btn.Name);
            TextBox_StatusAndSearch.Font = new Font("Consolas", 8);
            TextBox_StatusAndSearch.Text = btn.Name.Replace("\\drawings\\", string.Empty);
        }

        /// <summary>
        /// Event on Closing the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainFormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                // Settings created in Project>>Properties>>Settings:
                myUserSettings.WindowPosition = this.Location;
                myUserSettings.Save();
                kbh.Keyboard_Unhook();
            }
            catch { }          
        }

        /// <summary>
        /// Event on Loading / startup of the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainFormLoad(object sender, EventArgs e)
        {
            try
            {
                if (IsThisFirstRunAfterUpdate())
                {                  
                    //this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation;
                    this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
                    Properties.Settings.Default.userLastAppVersion = GetAppVersion();
                    Properties.Settings.Default.Save();
                    // Settings created in Project>>Properties>>Settings:
                    myUserSettings = new MyUserSettings();
                }
                else
                {
                    // See MyUserSettings.cs for SetUp
                    myUserSettings = new MyUserSettings();
                    this.StartPosition = FormStartPosition.Manual;
                    this.FormBorderStyle = FormBorderStyle.Fixed3D;

                    //this.Location = myUserSettings.WindowPosition;
                    this.DataBindings.Add(new Binding("Location", myUserSettings, "WindowPosition"));

                    this.ShowInTaskbar = false;
                    this.ShowInTaskbar = true;
                }
            }
            //catch (Exception)
            //{
            //    string userName = System.Environment.UserName;
            //    MessageBox.Show("Welcome new user:\n" + userName);
            //}
            catch { }
        }

        /// <summary>
        /// Checks if the current app version is the same as the last time, ...
        /// </summary>
        private bool IsThisFirstRunAfterUpdate()
        {
            if (Properties.Settings.Default.userLastAppVersion == GetAppVersion())
            {
                //MessageBox.Show(Properties.Settings.Default.userLastAppVersion.ToString() + ",\n" + GetAppVersion
                return false;
            }
            else
            {
                //MessageBox.Show(Properties.Settings.Default.userLastAppVersion.ToString() + ",\n" + GetAppVersion
                return true;
            }
        }

        //public void ShowStartupWelcomeScreen(string command)
        //{
        //    var swc = new StartupWelcomeScreen();
        //    switch (command)
        //    {
        //        case ("Show"):                    
        //            swc.Enabled = true;
        //            swc.Show();
        //            swc.TopMost = true;
        //            break;
        //        case ("Close"):
        //            var t = new System.Timers.Timer();
        //            t.Interval = 1000;
        //            t.Enabled = true;
        //            t.Elapsed += T_Elapsed;        
        //            break;
        //        case ("CloseNow"):
        //            swc.Close();
        //            swc.Hide();
        //            swc.Dispose();
        //            break;
        //    }
            
        //}

        //private void T_Elapsed(object sender, ElapsedEventArgs e)
        //{
        //    ShowStartupWelcomeScreen("CloseNow");
        //}

        /// <summary>
        /// Developer info form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Info_Click(object sender, EventArgs e)
        {
            DeveloperInfoForm devForm = new DeveloperInfoForm();
            devForm.Enabled = true;
            devForm.Show();
        }

        /// <summary>
        /// Delete macro name in the status text to allow searching
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBox_StatusAndSearch_Click(object sender, EventArgs e)
        {
            TextBox_StatusAndSearch.Text = "";
        }

        /// <summary>
        /// Sets hook for keyboard shortcuts
        /// </summary>
        private void SetKeyboardHook()
        {
            KeyboardHook kbh = new KeyboardHook();
            kbh.Keyboard_Hook();
        }

        private void LoadPluginImplementer()
        {
            try
            {
                //PluginImplementer PIM;
                string appRootFolder = GetApplicationRootFolder();

                foreach (var file in Directory.GetFiles(appRootFolder + "\\Plugins", "*.dll"))
                {
                    var assembly = Assembly.LoadFrom(file);
                    foreach (var type in assembly.GetTypes())
                    {
                        if (type.GetInterfaces().Contains(typeof(PluginImplementer)))
                        {
                            // Create Instance of the class that inherits from "PluginImplementer":
                            PluginImplementer PIM;
                            PIM = Activator.CreateInstance(type) as PluginImplementer;

                            // Create a new Plugin's tab:
                            var tab = new TabPage();
                            //tab.Size = PIM.RecomendedFrameSize();
                            PIM.SetupGUI_MainTab(tab);
                            Plugins_ControlBox.Visible = true;
                            Plugins_ControlBox.Controls.Add(tab);
                            //Plugins_ControlBox.Size = PIM.RecomendedFrameSize();
                            //Plugins_ControlBox.Size = new System.Drawing.Size(Plugins_ControlBox.Size.Width / 2, Plugins_ControlBox.Size.Height / 2);
                            tab.Layout += (sender, EventArgs) => { Tab_Click(sender, EventArgs, PIM.RecomendedFrameSize()); };

                            // Create GUI elements inside the tab:
                            PIM.SetupGUI_Objects();
                            List<System.Object> listGUIObjects = new List<object>();
                            listGUIObjects = PIM.listGUIObjects();
                            foreach (dynamic b in listGUIObjects)
                            {
                                if (b != null)
                                {
                                    tab.Controls.Add(b as Control);
                                    b.Scale(new SizeF(0.5f, 0.5f));
                                }
                            }
                        }
                    }
                }
            }
            //catch (Exception ex) { MessageBox.Show(ex.Message); }     // Plugins folder doesn't exist
            catch { }
        }

        /// <summary>
        /// Refresh the Form Size when clicked on the Plugin's Tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="frameSize"></param>
        private void Tab_Click(object sender, EventArgs e, System.Drawing.Size frameSize)
        {
            frameSize = new System.Drawing.Size(frameSize.Width / 2, frameSize.Height / 2);
            Plugins_ControlBox.Size = frameSize;
            FormSize_basedOn_TabControlBoxSubtab(frameSize);
        }

        // Delete this later
        private void button1_Click(object sender, EventArgs e)
        {
            LoadPluginImplementer();
        }

        private void PluginButton_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;
            Console.WriteLine(b.Name);
        }

        private void ExitApplication()
        {
            kbh.Keyboard_Unhook();
            Environment.Exit(1);
        }

        public string GetAppVersion()
        {
            try
            {
                return System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            }
            catch (Exception ex)
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        private void print(string text)
        {
            Console.WriteLine(text);
        }
    }
}
