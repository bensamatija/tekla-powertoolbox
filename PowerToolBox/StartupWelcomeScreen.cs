﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Tekla:
using T = Tekla.Structures;
using TSM = Tekla.Structures.Model;
using TSMUI = Tekla.Structures.Model.UI;
//using System.Collections;
using T3D = Tekla.Structures.Geometry3d;
//using Tekla.Technology.MacroSelector;
//using Tekla.Technology.Akit;
//using Tekla.Macros;
//using Tekla.Macros.Runtime;
//using Tekla.Technology.Scripting;

// Others:
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Configuration;
using System.Timers;
using System.Diagnostics;

namespace Tekla___PowerToolBox
{
    public partial class StartupWelcomeScreen : Form
    {
        public StartupWelcomeScreen()
        {
            InitializeComponent();
        }

        private void StartupWelcomeScreen_Load(object sender, EventArgs e)
        {
            appNameVersion.Text = "PowerToolBox " + GetAppVersion();
            pictureBox1.Location = new System.Drawing.Point(this.ClientSize.Width / 2 - pictureBox1.Size.Width / 2, this.ClientSize.Height - pictureBox1.Size.Height - 50);
        }

        private string GetAppVersion()
        {
            try
            {
                return System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            }
            catch (Exception ex)
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public void Close()
        {
            this.ActiveControl = null;
        }
    }
}
