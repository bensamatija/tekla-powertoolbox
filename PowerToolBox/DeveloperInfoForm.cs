﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tekla___PowerToolBox
{
    public partial class DeveloperInfoForm : Form
    {
        //public string appVersion = System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
        public DeveloperInfoForm()
        {
            InitializeComponent();
            DeveloperInfoWindowProperties();
            DeveloperInfoSet();
            GetAppVersion();
        }

        public void DeveloperInfoSet()
        {
            string appVersion = GetAppVersion();
            btn_devInfoOk.Location = new System.Drawing.Point(this.ClientSize.Width / 2 - btn_devInfoOk.Size.Width / 2, this.ClientSize.Height - btn_devInfoOk.Height - 5);
            pictureBox1.Location = new System.Drawing.Point(this.ClientSize.Width / 2 - pictureBox1.Size.Width / 2, this.ClientSize.Height - pictureBox1.Size.Height - btn_devInfoOk.Height - 5);

            devLabel1.Text =
                "Company: DS Skanding" +
                "\nDeveloper: Matija Bensa " +
                "\nEmail: matija.bensa@gmail.com" +
                "\nYear: 2017" +
                //"\nVersion: " + this.ProductVersion.ToString();     //AssemblyFileVersion
                "\nVersion: " + appVersion;     //AssemblyFileVersion


        }

        private string GetAppVersion()
        {
            try
            {
                return System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            }
            catch (Exception ex)
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        private void DeveloperInfoWindowProperties()
        {
            this.ClientSize = new System.Drawing.Size(220, 200);
        }

        private void btn_devInfoOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DeveloperInfoForm_Load(object sender, EventArgs e)
        {

        }
    }
}
