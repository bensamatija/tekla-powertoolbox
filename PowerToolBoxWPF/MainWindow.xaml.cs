﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Windows.Forms;
using System.Xaml;

// Tekla:
using T = Tekla.Structures;
using TSM = Tekla.Structures.Model;
using TSMUI = Tekla.Structures.Model.UI;
//using System.Collections;
using T3D = Tekla.Structures.Geometry3d;
//using Tekla.Technology.MacroSelector;
//using Tekla.Technology.Akit;
//using Tekla.Macros;
//using Tekla.Macros.Runtime;
//using Tekla.Technology.Scripting;

// Others:
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Configuration;
using System.Timers;
using System.Diagnostics;

namespace PowerToolBoxWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            RestoreWindowPosition();
        }

        private void RestoreWindowPosition()
        {
            //this.Top = Properties.Settings.Default.Top;
            //this.Left = Properties.Settings.Default.Left;
            //this.Height = Properties.Settings.Default.Height;
            //this.Width = Properties.Settings.Default.Width;
            //// Very quick and dirty - but it does the job
            //if (Properties.Settings.Default.Maximized)
            //{
            //    WindowState = WindowState.Maximized;
            //}
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            //var ww = Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.IsActive);
            var main = App.Current.MainWindow as MainWindow;
            //Properties.Settings.Default.WindowPosition = WindowStateHelper.ToXml();
        }
    }
}
