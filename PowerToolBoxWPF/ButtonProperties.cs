﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Windows.Forms;

// Others:
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Drawing;


namespace Tekla___PowerToolBox
{
    [XmlRoot("Root")]
    public class ButtonProperties
    {
        //[XmlAttribute("buttonName")]
        [XmlIgnore]
        public string buttonName { get; set; }
        [XmlAttribute("macro")]
        public string macro { get; set; }
        //[XmlElement("text")]
        //public string text { get; set; }
        [XmlElement("image")]
        public string image { get; set; }
        //[XmlElement("pX")]
        [XmlIgnore]
        public int pX { get; set; }
        //[XmlElement("pY")]
        [XmlIgnore]
        public int pY { get; set; }
        [XmlElement("row")]
        public int row { get; set; }
        [XmlElement("col")]
        public int col { get; set; }
    }
}
