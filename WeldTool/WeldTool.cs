﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Tekla:
using T = Tekla.Structures;
using TSM = Tekla.Structures.Model;
using TSMUI = Tekla.Structures.Model.UI;
//using System.Collections;
using T3D = Tekla.Structures.Geometry3d;
//using Tekla.Technology.MacroSelector;
//using Tekla.Technology.Akit;
//using Tekla.Macros;
//using Tekla.Macros.Runtime;
//using Tekla.Technology.Scripting;

// Others:
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Configuration;
using System.Timers;
using System.Diagnostics;

namespace WeldTool
{
    class WeldTool
    {
        public string appName = "WeldTool";

        // Get the current model:
        TSM.Model model = new TSM.Model();

        /// <summary>
        /// Return the type of the first weld in the queue so the button is always effective
        /// </summary>
        /// <param name="moe"></param>
        /// <returns></returns>
        private int ReturnFirstWeldType(TSM.ModelObjectEnumerator moe)
        {
            int wType = 0;
            foreach (TSM.Object o in moe)
            {
                TSM.Weld w = o as TSM.Weld;
                if (w != null)
                {
                    if (w.AroundWeld == true)
                    {
                        return wType = 1;
                    }
                    else if (w.AroundWeld == false)
                    {
                        return wType = 2;
                    }
                }
            }
            return 0;
        }

        /// <summary>
        /// Cicle the type of the weld for selected welds
        /// </summary>
        /// <param name="wSize"></param>
        public void CicleWeld(int wSize)
        {
            // Get Selected Objects:
            TSMUI.ModelObjectSelector mos = new TSMUI.ModelObjectSelector();
            TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();

            // Detect 1st weld type:
            int wType = ReturnFirstWeldType(moe);
            moe.Reset();

            if (wType == 1)
            {
                foreach (TSM.Object o in moe)
                {
                    TSM.Weld w = o as TSM.Weld;
                    if (w != null)
                    {
                        w.AroundWeld = false;
                        w.SizeAbove = wSize;
                        w.SizeBelow = wSize;
                        w.TypeAbove = TSM.BaseWeld.WeldTypeEnum.WELD_TYPE_FILLET;
                        w.TypeBelow = TSM.BaseWeld.WeldTypeEnum.WELD_TYPE_FILLET;
                        w.Modify();
                    }
                }
                model.CommitChanges();
                return;
            }
            if (wType == 2)
            {
                foreach (TSM.Object o in moe)
                {
                    TSM.Weld w = o as TSM.Weld;
                    if (w != null)
                    {
                        w.AroundWeld = true;
                        w.SizeAbove = wSize;
                        w.SizeBelow = 0;
                        w.TypeAbove = TSM.BaseWeld.WeldTypeEnum.WELD_TYPE_FILLET;
                        w.TypeBelow = TSM.BaseWeld.WeldTypeEnum.WELD_TYPE_NONE;
                        w.Modify();
                    }
                }
                model.CommitChanges();
                return;
            }
        }

        /// <summary>
        /// Finds welds with not correct marks and tries to fix them
        /// </summary>
        public void FixWelds()
        {
            // Get Selected Objects:
            TSMUI.ModelObjectSelector mos = new TSMUI.ModelObjectSelector();
            TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();

            var list_weldsExceptionsNotChanged = new System.Collections.ArrayList();


            foreach (TSM.Object o in moe)
            {
                TSM.Weld w = o as TSM.Weld;
                if (w != null)
                {
                    if (w.TypeAbove.Equals(TSM.BaseWeld.WeldTypeEnum.WELD_TYPE_FILLET))
                    {
                        if (w.TypeBelow.Equals(TSM.BaseWeld.WeldTypeEnum.WELD_TYPE_FILLET))
                        {
                            if (w.AroundWeld == true)
                            {
                                if (w.SizeAbove == w.SizeBelow)
                                {
                                    // FIX:
                                    w.TypeBelow = TSM.BaseWeld.WeldTypeEnum.WELD_TYPE_NONE;
                                    w.SizeBelow = 0;
                                    w.Modify();
                                    list_weldsExceptionsNotChanged.Add(w);
                                }
                            }
                        }
                        if (w.TypeBelow.Equals(TSM.BaseWeld.WeldTypeEnum.WELD_TYPE_NONE))
                        {
                            if (w.AroundWeld == false)
                            {
                                if (w.SizeBelow == 0)
                                {
                                    // FIX:
                                    w.AroundWeld = true;
                                    w.Modify();
                                    list_weldsExceptionsNotChanged.Add(w);
                                }
                            }
                        }
                    }
                }
            }
            // Show Welds that couldn't be safely changed:         
            if (list_weldsExceptionsNotChanged.Count > 0)
            {
                mos.Select(list_weldsExceptionsNotChanged);
                var result = MessageBox.Show("Selected weld(s) have been fixed.", appName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            if (list_weldsExceptionsNotChanged.Count == 0)
            {
                var result = MessageBox.Show("Selected weld(s) couldn't be safely changed.", appName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            model.CommitChanges();
        }

        ///// <summary>
        ///// Switches Weld Type to different than it was before
        ///// </summary>
        //public void SwitchWelds()
        //{
        //    // Get Selected Objects:
        //    TSMUI.ModelObjectSelector mos = new TSMUI.ModelObjectSelector();
        //    TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();

        //    foreach (TSM.Weld w in moe)
        //    {
        //        print(w.SizeAbove.ToString());
        //        if (w.TypeAbove.Equals(TSM.Weld.WeldTypeEnum.WELD_TYPE_FILLET))
        //        {
        //            if (w.TypeBelow.Equals(TSM.Weld.WeldTypeEnum.WELD_TYPE_FILLET))
        //            {
        //                // Switch to /°-
        //                w.AroundWeld = true;
        //                w.TypeBelow = TSM.BaseWeld.WeldTypeEnum.WELD_TYPE_NONE;
        //                w.SizeBelow = 0;
        //                w.Modify();
        //            }
        //            else
        //            {
        //                // Switch to /-
        //                w.AroundWeld = false;
        //                w.SizeBelow = w.SizeAbove;
        //                w.TypeBelow = TSM.Weld.WeldTypeEnum.WELD_TYPE_FILLET;
        //                w.Modify();
        //            }
        //        }
        //    }
        //    model.CommitChanges();
        //}
    }
}
