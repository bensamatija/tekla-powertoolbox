﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Tekla:
using T = Tekla.Structures;
using TSM = Tekla.Structures.Model;
using TSMUI = Tekla.Structures.Model.UI;
//using System.Collections;
using T3D = Tekla.Structures.Geometry3d;
//using Tekla.Technology.MacroSelector;
//using Tekla.Technology.Akit;
//using Tekla.Macros;
//using Tekla.Macros.Runtime;
//using Tekla.Technology.Scripting;

// Others:
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Configuration;
using System.Timers;
using System.Diagnostics;

using PluginInterface;

namespace WeldTool
{
    class WeldToolInterface : PluginImplementer
    {
        public List<System.Object> listGUIObjects = new List<object>();

        List<object> PluginImplementer.listGUIObjects() { return listGUIObjects; }

        public System.Drawing.Size RecomendedFrameSize() { return new System.Drawing.Size(660, 415); }

        public void SetupGUI_MainTab(TabPage tab)
        {
            tab.Size = new System.Drawing.Size(40, 40);
            tab.Text = "Weld Tool";
            //tab.Name = "plugin name";
            tab.BackColor = Color.White;
        }

        public void InitializeComponentDLL()
        {
            //ConnectToModel();
        }

        public void SetupGUI_Objects()
        {
            this.CicleWeldType_group = new System.Windows.Forms.GroupBox();
            this.CicleWeldType_3 = new System.Windows.Forms.Button();
            this.CicleWeldType_8 = new System.Windows.Forms.Button();
            this.CicleWeldType_7 = new System.Windows.Forms.Button();
            this.CicleWeldType_6 = new System.Windows.Forms.Button();
            this.CicleWeldType_5 = new System.Windows.Forms.Button();
            this.CicleWeldType_4 = new System.Windows.Forms.Button();
            this.WeldsOther = new System.Windows.Forms.GroupBox();
            this.Btn_FixWelds = new System.Windows.Forms.Button();
            //this.CicleWeldType.SuspendLayout();
            //this.WeldsOther.SuspendLayout();
            //this.SuspendLayout();
            // 
            // CicleWeldType
            // 
            this.CicleWeldType_group.Controls.Add(this.CicleWeldType_3);
            this.CicleWeldType_group.Controls.Add(this.CicleWeldType_8);
            this.CicleWeldType_group.Controls.Add(this.CicleWeldType_7);
            this.CicleWeldType_group.Controls.Add(this.CicleWeldType_6);
            this.CicleWeldType_group.Controls.Add(this.CicleWeldType_5);
            this.CicleWeldType_group.Controls.Add(this.CicleWeldType_4);
            this.CicleWeldType_group.Font = new System.Drawing.Font("Consolas", 7.875F);
            this.CicleWeldType_group.Location = new System.Drawing.Point(0, 5);
            this.CicleWeldType_group.Name = "CicleWeldType";
            this.CicleWeldType_group.Size = new System.Drawing.Size(642, 150);
            this.CicleWeldType_group.TabIndex = 1;
            this.CicleWeldType_group.TabStop = false;
            this.CicleWeldType_group.Text = "Cicle Weld Type";
            // 
            // CicleWeldType_3
            // 
            this.CicleWeldType_3.Font = new System.Drawing.Font("Consolas", 9F);
            this.CicleWeldType_3.Location = new System.Drawing.Point(6, 31);
            this.CicleWeldType_3.Name = "CicleWeldType_3";
            this.CicleWeldType_3.Size = new System.Drawing.Size(100, 100);
            this.CicleWeldType_3.TabIndex = 7;
            this.CicleWeldType_3.Text = "3";
            this.CicleWeldType_3.UseVisualStyleBackColor = true;
            this.CicleWeldType_3.Click += CicleWeldType_3_Click;
            //this.CicleWeldType_3.Click += new System.EventHandler(this.CicleWeldType_3_Click);
            // 
            // CicleWeldType_8
            // 
            this.CicleWeldType_8.Font = new System.Drawing.Font("Consolas", 9F);
            this.CicleWeldType_8.Location = new System.Drawing.Point(536, 31);
            this.CicleWeldType_8.Name = "CicleWeldType_8";
            this.CicleWeldType_8.Size = new System.Drawing.Size(100, 100);
            this.CicleWeldType_8.TabIndex = 6;
            this.CicleWeldType_8.Text = "8";
            this.CicleWeldType_8.UseVisualStyleBackColor = true;
            this.CicleWeldType_8.Click += CicleWeldType_8_Click;
            //this.CicleWeldType_8.Click += new System.EventHandler(this.CicleWeldType_8_Click);
            // 
            // CicleWeldType_7
            // 
            this.CicleWeldType_7.Font = new System.Drawing.Font("Consolas", 9F);
            this.CicleWeldType_7.Location = new System.Drawing.Point(430, 31);
            this.CicleWeldType_7.Name = "CicleWeldType_7";
            this.CicleWeldType_7.Size = new System.Drawing.Size(100, 100);
            this.CicleWeldType_7.TabIndex = 5;
            this.CicleWeldType_7.Text = "7";
            this.CicleWeldType_7.UseVisualStyleBackColor = true;
            this.CicleWeldType_7.Click += CicleWeldType_7_Click;
            //this.CicleWeldType_7.Click += new System.EventHandler(this.CicleWeldType_7_Click);
            // 
            // CicleWeldType_6
            // 
            this.CicleWeldType_6.Font = new System.Drawing.Font("Consolas", 9F);
            this.CicleWeldType_6.Location = new System.Drawing.Point(324, 31);
            this.CicleWeldType_6.Name = "CicleWeldType_6";
            this.CicleWeldType_6.Size = new System.Drawing.Size(100, 100);
            this.CicleWeldType_6.TabIndex = 4;
            this.CicleWeldType_6.Text = "6";
            this.CicleWeldType_6.UseVisualStyleBackColor = true;
            this.CicleWeldType_6.Click += CicleWeldType_6_Click;
            //this.CicleWeldType_6.Click += new System.EventHandler(this.CicleWeldType_6_Click);
            // 
            // CicleWeldType_5
            // 
            this.CicleWeldType_5.Font = new System.Drawing.Font("Consolas", 9F);
            this.CicleWeldType_5.Location = new System.Drawing.Point(218, 31);
            this.CicleWeldType_5.Name = "CicleWeldType_5";
            this.CicleWeldType_5.Size = new System.Drawing.Size(100, 100);
            this.CicleWeldType_5.TabIndex = 3;
            this.CicleWeldType_5.Text = "5";
            this.CicleWeldType_5.UseVisualStyleBackColor = true;
            this.CicleWeldType_5.Click += CicleWeldType_5_Click;
            //this.CicleWeldType_5.Click += new System.EventHandler(this.CicleWeldType_5_Click);
            // 
            // CicleWeldType_4
            // 
            this.CicleWeldType_4.Font = new System.Drawing.Font("Consolas", 9F);
            this.CicleWeldType_4.Location = new System.Drawing.Point(112, 31);
            this.CicleWeldType_4.Name = "CicleWeldType_4";
            this.CicleWeldType_4.Size = new System.Drawing.Size(100, 100);
            this.CicleWeldType_4.TabIndex = 2;
            this.CicleWeldType_4.Text = "4";
            this.CicleWeldType_4.UseVisualStyleBackColor = true;
            this.CicleWeldType_4.Click += CicleWeldType_4_Click;
            //this.CicleWeldType_4.Click += new System.EventHandler(this.CicleWeldType_4_Click);
            // 
            // WeldsOther
            // 
            this.WeldsOther.Controls.Add(this.Btn_FixWelds);
            this.WeldsOther.Font = new System.Drawing.Font("Consolas", 7.875F);
            this.WeldsOther.Location = new System.Drawing.Point(0, 168);
            this.WeldsOther.Name = "WeldsOther";
            this.WeldsOther.Size = new System.Drawing.Size(642, 150);
            this.WeldsOther.TabIndex = 2;
            this.WeldsOther.TabStop = false;
            this.WeldsOther.Text = "Other settings";
            // 
            // Btn_FixWelds
            // 
            this.Btn_FixWelds.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Btn_FixWelds.Font = new System.Drawing.Font("Consolas", 7.875F);
            //this.Btn_FixWelds.Image = ((System.Drawing.Image)(resources.GetObject("Btn_FixWelds.Image")));
            this.Btn_FixWelds.Location = new System.Drawing.Point(6, 31);
            this.Btn_FixWelds.Name = "Btn_FixWelds";
            this.Btn_FixWelds.Size = new System.Drawing.Size(100, 100);
            this.Btn_FixWelds.TabIndex = 1;
            this.Btn_FixWelds.Text = "Fix Welds";
            this.Btn_FixWelds.UseVisualStyleBackColor = true;
            this.Btn_FixWelds.Click += Btn_FixWelds_Click;

            // Add all the GUI Objects to the list:
            listGUIObjects.Add(CicleWeldType_group);
            //listGUIObjects.Add(CicleWeldType_3);
            //listGUIObjects.Add(CicleWeldType_8);
            //listGUIObjects.Add(CicleWeldType_7);
            //listGUIObjects.Add(CicleWeldType_6);
            //listGUIObjects.Add(CicleWeldType_5);
            //listGUIObjects.Add(CicleWeldType_4);
            listGUIObjects.Add(WeldsOther);
            //listGUIObjects.Add(Btn_FixWelds);
            //listGUIObjects.Add(CicleWeldType);
            //listGUIObjects.Add(WeldsOther);



            //this.Btn_FixWelds.Click += new System.EventHandler(this.Btn_FixWelds_Click);
            //// 
            //// WeldTool
            //// 
            //this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            //this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            //this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            //this.ClientSize = new System.Drawing.Size(668, 333);
            //this.Controls.Add(this.WeldsOther);
            //this.Controls.Add(this.CicleWeldType);
            //this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            //this.Name = "WeldTool";
            //this.Text = "WeldTool";
            //this.TopMost = true;
            //this.CicleWeldType.ResumeLayout(false);
            //this.WeldsOther.ResumeLayout(false);
            //this.ResumeLayout(false);

        }
        /// <summary>
        /// Events for each button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CicleWeldType_3_Click(object sender, EventArgs e) { WeldTool WT = new WeldTool(); WT.CicleWeld(3); }
        private void CicleWeldType_4_Click(object sender, EventArgs e) { WeldTool WT = new WeldTool(); WT.CicleWeld(4); }
        private void CicleWeldType_5_Click(object sender, EventArgs e) { WeldTool WT = new WeldTool(); WT.CicleWeld(5); }
        private void CicleWeldType_6_Click(object sender, EventArgs e) { WeldTool WT = new WeldTool(); WT.CicleWeld(6); }
        private void CicleWeldType_7_Click(object sender, EventArgs e) { WeldTool WT = new WeldTool(); WT.CicleWeld(7); }
        private void CicleWeldType_8_Click(object sender, EventArgs e) { WeldTool WT = new WeldTool(); WT.CicleWeld(8); }

        private void Btn_FixWelds_Click(object sender, EventArgs e) { WeldTool WT = new WeldTool(); WT.FixWelds(); }

        //#endregion
        private System.Windows.Forms.GroupBox CicleWeldType_group;
        private System.Windows.Forms.GroupBox WeldsOther;
        private System.Windows.Forms.Button Btn_FixWelds;
        private System.Windows.Forms.Button CicleWeldType_4;
        private System.Windows.Forms.Button CicleWeldType_5;
        private System.Windows.Forms.Button CicleWeldType_8;
        private System.Windows.Forms.Button CicleWeldType_7;
        private System.Windows.Forms.Button CicleWeldType_6;
        private System.Windows.Forms.Button CicleWeldType_3;
    }



}
